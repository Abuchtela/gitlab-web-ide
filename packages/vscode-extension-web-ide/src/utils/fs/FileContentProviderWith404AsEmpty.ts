import type { IFileContentProvider } from '@gitlab/web-ide-fs';

/**
 * Decorator for IFileContentProvider that returns 404 responses as empty
 */
export class FileContentProviderWith404AsEmpty implements IFileContentProvider {
  readonly #base: IFileContentProvider;

  constructor(base: IFileContentProvider) {
    this.#base = base;
  }

  async getContent(path: string): Promise<Uint8Array> {
    try {
      // note: ESLint doesn't like it if I just return here...
      const value = await this.#base.getContent(path);

      return value;
    } catch (e: unknown) {
      if (e && e instanceof Error && e.message.match('404')) {
        return new Uint8Array(0);
      }

      throw e;
    }
  }
}
