import { startRemote as startRemoteMediator } from '../mediator';
import * as vscodeUi from '../vscodeUi';

const HOSTNAME_VALID_REGEX = /^[\w.:-]+$/;
const HOSTNAME_INVALID_MSG =
  "Host name is invalid. The hostname can only include alphanumeric chatacters the special characters '.', '-', and ':'.";

export default async (): Promise<void> => {
  const commonInputBoxProps = {
    title: 'Connect to a remote environment',
    ignoreFocusOut: true,
  };

  const remoteHostResponse = await vscodeUi.showInputBox({
    ...commonInputBoxProps,
    prompt: 'Remote host',
    placeholder: 'remote.mydomain.com',
    step: 1,
    totalSteps: 3,
    async validate(val) {
      if (!val) {
        return 'Remote host is required and cannot be empty.';
      }
      if (!HOSTNAME_VALID_REGEX.test(val)) {
        return HOSTNAME_INVALID_MSG;
      }

      return undefined;
    },
  });

  if (remoteHostResponse.canceled) {
    return;
  }

  const remotePathResponse = await vscodeUi.showInputBox({
    ...commonInputBoxProps,
    prompt: `File path to repository on ${remoteHostResponse.value}`,
    placeholder: '/example/project',
    step: 2,
    totalSteps: 3,
    async validate(val) {
      if (!val) {
        return 'Path is required and must be an absolute path starting with `/`';
      }
      if (!val.startsWith('/')) {
        return 'Path must be an absolute path and start with `/`';
      }

      return undefined;
    },
  });

  if (remotePathResponse.canceled) {
    return;
  }

  const connectionTokenResponse = await vscodeUi.showInputBox({
    ...commonInputBoxProps,
    prompt: 'Connection token',
    password: true,
    step: 3,
    totalSteps: 3,
  });

  if (connectionTokenResponse.canceled) {
    return;
  }

  await startRemoteMediator({
    remoteHost: remoteHostResponse.value,
    remotePath: remotePathResponse.value,
    connectionToken: connectionTokenResponse.value,
  });
};
