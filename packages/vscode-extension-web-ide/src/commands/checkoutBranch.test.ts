import * as vscode from 'vscode';
import checkoutBranch, {
  MSG_SELECT_BRANCH,
  ITEM_CREATE_NEW,
  MSG_ERROR_SEARCH_BRANCH,
  ITEM_NOT_FOUND,
} from './checkoutBranch';
import { fetchProjectBranches, createProjectBranch } from '../mediator';
import { showInputBox, showSearchableQuickPick } from '../vscodeUi';
import { TEST_COMMANDS_INITIAL_DATA } from '../../test-utils';
import { RELOAD_WITH_WARNING_COMMAND_ID } from '../constants';

jest.mock('../mediator');
jest.mock('../vscodeUi');

const TEST_BRANCHES = ['foo', 'foo-4', 'foo-bar'];
const ITEMS_TEST_BRANCHES = TEST_BRANCHES.map(name => ({
  alwaysShow: false,
  label: `$(git-branch) ${name}`,
  ref: name,
  type: 'existing-branch',
}));

describe('commands/checkoutBranch', () => {
  const getSearchableQuickPickOptions = () => jest.mocked(showSearchableQuickPick).mock.calls[0][0];

  const getInputBoxOptions = () => jest.mocked(showInputBox).mock.calls[0][0];

  beforeEach(() => {
    jest.mocked(showSearchableQuickPick).mockResolvedValue(undefined);
    jest.mocked(showInputBox).mockResolvedValue({ canceled: true });
    jest.mocked(fetchProjectBranches).mockResolvedValue(TEST_BRANCHES);
  });

  describe('default (nothing selected)', () => {
    beforeEach(async () => {
      await checkoutBranch(Promise.resolve(TEST_COMMANDS_INITIAL_DATA))();
    });

    it('shows quick pick', () => {
      expect(showSearchableQuickPick).toHaveBeenCalledWith({
        placeholder: MSG_SELECT_BRANCH,
        defaultItems: [ITEM_CREATE_NEW],
        searchItems: expect.any(Function),
        handleSearchError: expect.any(Function),
      });
    });

    it('does not call any other commands', () => {
      expect(vscode.commands.executeCommand).not.toHaveBeenCalled();
    });

    describe('searchItems', () => {
      let result: vscode.QuickPickItem[];

      describe('with search pattern', () => {
        beforeEach(async () => {
          result = await getSearchableQuickPickOptions().searchItems('foo');
        });

        it('calls command with and without wildcards', () => {
          expect(jest.mocked(fetchProjectBranches).mock.calls).toEqual([
            [
              {
                projectPath: TEST_COMMANDS_INITIAL_DATA.project.path_with_namespace,
                searchPattern: 'foo',
                offset: 0,
                limit: 1,
              },
            ],
            [
              {
                projectPath: TEST_COMMANDS_INITIAL_DATA.project.path_with_namespace,
                searchPattern: '*foo*',
                offset: 0,
                limit: 100,
              },
            ],
          ]);
        });

        it('returns items including default', () => {
          expect(result).toEqual([ITEM_CREATE_NEW, ...ITEMS_TEST_BRANCHES]);
        });
      });

      describe('without search pattern', () => {
        beforeEach(async () => {
          result = await getSearchableQuickPickOptions().searchItems('');
        });

        it('calls command with *', () => {
          expect(fetchProjectBranches).toHaveBeenCalledWith({
            projectPath: TEST_COMMANDS_INITIAL_DATA.project.path_with_namespace,
            searchPattern: '*',
            offset: 0,
            limit: 100,
          });
        });
      });
    });

    describe('handleSearchError', () => {
      const TEST_ERROR = new Error('TEST ERROR');

      beforeEach(() => {
        jest.spyOn(console, 'error').mockImplementation();
        const { handleSearchError } = getSearchableQuickPickOptions();

        handleSearchError?.(TEST_ERROR);
      });

      it('shows warning message', () => {
        expect(vscode.window.showWarningMessage).toHaveBeenCalledWith(MSG_ERROR_SEARCH_BRANCH);
      });

      it('logs error to console', () => {
        // eslint-disable-next-line no-console
        expect(console.error).toHaveBeenCalledWith(
          '[gitlab-webide] Error occurred while searching for branches',
          TEST_ERROR,
        );
      });
    });
  });

  describe('when user selects existing branch', () => {
    beforeEach(async () => {
      jest.mocked(showSearchableQuickPick).mockResolvedValue(ITEMS_TEST_BRANCHES[1]);

      await checkoutBranch(Promise.resolve(TEST_COMMANDS_INITIAL_DATA))();
    });

    it('executes RELOAD_WITH_WARNING command', () => {
      expect(vscode.commands.executeCommand).toHaveBeenCalledWith(RELOAD_WITH_WARNING_COMMAND_ID, {
        message: 'Are you sure you want to checkout "foo-4"? Any unsaved changes will be lost.',
        okText: 'Yes',
        ref: 'foo-4',
      });
    });
  });

  describe('when user cannot push code', () => {
    beforeEach(async () => {
      await checkoutBranch(
        Promise.resolve({
          ...TEST_COMMANDS_INITIAL_DATA,
          userPermissions: { pushCode: false, createMergeRequestIn: true, readMergeRequest: true },
        }),
      )();
    });

    it('does not show "create branch" item', () => {
      expect(showSearchableQuickPick).toHaveBeenCalledWith(
        expect.objectContaining({
          defaultItems: [],
        }),
      );
    });

    it('does not show "create branch" item when searching', async () => {
      const result = await getSearchableQuickPickOptions().searchItems('foo');

      // for defensiveness, assert the lack of ITEM_CREATE_NEW
      expect(result).not.toContain(ITEM_CREATE_NEW);
      expect(result).toEqual(ITEMS_TEST_BRANCHES);
    });

    it('shows a "not found" item when search returns no results', async () => {
      jest.mocked(fetchProjectBranches).mockResolvedValue([]);

      const result = await getSearchableQuickPickOptions().searchItems('foo');

      expect(result).toEqual([ITEM_NOT_FOUND]);
    });
  });

  describe('when create branch is selected and canceled', () => {
    beforeEach(async () => {
      jest.mocked(showSearchableQuickPick).mockResolvedValue(ITEM_CREATE_NEW);

      await checkoutBranch(Promise.resolve(TEST_COMMANDS_INITIAL_DATA))();
    });

    it('does not execute any commands', () => {
      expect(vscode.commands.executeCommand).not.toHaveBeenCalled();
    });

    it('shows create branch prompt', () => {
      expect(showInputBox).toHaveBeenCalledWith({
        placeholder: 'Branch name',
        prompt: 'Please provide a new branch name',
        validate: expect.any(Function),
      });
    });

    describe.each`
      desc                    | branchName | createResponse              | expectedCreateParams                           | expected
      ${'with empty branch'}  | ${''}      | ${{}}                       | ${undefined}                                   | ${'Branch name cannot be empty.'}
      ${'with branch'}        | ${'abc'}   | ${{}}                       | ${{ ref: TEST_COMMANDS_INITIAL_DATA.ref.sha }} | ${undefined}
      ${'with create errors'} | ${'abc'}   | ${{ errors: ['Problem!'] }} | ${{ ref: TEST_COMMANDS_INITIAL_DATA.ref.sha }} | ${'Problem!'}
    `('validate - $desc', ({ branchName, expectedCreateParams, createResponse, expected }) => {
      let actual: Promise<string | vscode.InputBoxValidationMessage | undefined>;

      beforeEach(() => {
        jest.mocked(createProjectBranch).mockResolvedValue(createResponse);
        actual = getInputBoxOptions().validate?.(branchName) || Promise.reject();
      });

      it('resolves', async () => {
        await expect(actual).resolves.toBe(expected);
      });

      if (!expectedCreateParams) {
        it('does not call createProjectBranch', () => {
          expect(createProjectBranch).not.toHaveBeenCalled();
        });
      } else {
        it('calls createProjectBranch', () => {
          expect(createProjectBranch).toHaveBeenCalledWith({
            name: branchName,
            projectPath: TEST_COMMANDS_INITIAL_DATA.project.path_with_namespace,
            ...expectedCreateParams,
          });
        });
      }
    });
  });

  describe('when create branch is selected, entered, and created', () => {
    beforeEach(async () => {
      jest.mocked(showSearchableQuickPick).mockResolvedValue(ITEM_CREATE_NEW);
      jest.mocked(showInputBox).mockResolvedValue({ canceled: false, value: 'new-branch' });

      await checkoutBranch(Promise.resolve(TEST_COMMANDS_INITIAL_DATA))();
    });

    it('executes command to reload with warning', () => {
      expect(vscode.commands.executeCommand).toHaveBeenCalledWith(RELOAD_WITH_WARNING_COMMAND_ID, {
        message: `Are you sure you want to checkout "new-branch"? Any unsaved changes will be lost.`,
        okText: 'Yes',
        ref: 'new-branch',
      });
    });
  });

  describe('when project is empty repo', () => {
    beforeEach(async () => {
      jest.mocked(showSearchableQuickPick).mockResolvedValue(ITEM_CREATE_NEW);

      await checkoutBranch(
        Promise.resolve({
          ...TEST_COMMANDS_INITIAL_DATA,
          project: { ...TEST_COMMANDS_INITIAL_DATA.project, empty_repo: true },
        }),
      )();
    });

    it('does not try to create a branch', async () => {
      const result = await getInputBoxOptions().validate?.('new-branch');

      expect(createProjectBranch).not.toHaveBeenCalled();
      expect(result).toBeUndefined();
    });
  });
});
